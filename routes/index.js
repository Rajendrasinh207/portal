var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = process.env['PORTAL_BUILD_PATH'];
/* GET home page. */
router.get('/api', function(req, res, next) {
  var clients = [];
  fs.readdir(path,function(err,items){
    for (var index = 0; index < items.length; index++) {
      clients.push(items[index]);
    }
    res.status(200).json({clients:clients});
  });
});

router.get('*',function(req,res){
  res.sendfile('./public/index.html');
});
module.exports = router;
