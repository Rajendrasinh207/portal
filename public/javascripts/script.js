var portalApp = angular.module('portalApp', ['ngRoute']);

portalApp.config(['$routeProvider',function($routeProvider){
    $routeProvider

        .when('/',{
            templateUrl : 'views/home.html',
            controller  : 'homeController'
        })

        .when('/prefs',{
            templateUrl : 'views/prefs.html',
            controller  : 'prefsController'
        });
}]);

portalApp.controller('mainController', ['$scope','$http',function($scope,$http){
    $scope.message = "Everyone come and see this";
    $http.get('/api')
        .success(function(clients){
        $scope.clients = clients['clients'];
    })
    .error(function(err){
        console.log(err);
    });
}]);

portalApp.controller('homeController',['$scope',function($scope){
    $scope.message = "Everyone come and see this Home page";
}]);

portalApp.controller('prefsController',['$scope',function($scope){
    $scope.message = "Everyone come and see this Prefs page";
}]);